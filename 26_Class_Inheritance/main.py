class Animal:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def introduction(self):
        return "Hi, I'm {0} and I have {1} y.o".format(self.name, self.age)

class Dog(Animal):
    def voice(self):
        return "How How"

class Wolf(Dog):
    def getVoice(self):
        return super().voice()

class Cat(Animal):
    def getVoice(self):
        return "Meow Meow"

dog = Dog("Rexio", 2)
cat = Cat("Bury", 8)
wolf = Wolf("I'm wolf", 10)

print(dog.introduction(), "Dog has voice: ", dog.voice())
print(cat.introduction(), "Cat has voice: ", cat.getVoice())
print(wolf.introduction(), "Wolf has voice: ", wolf.getVoice())