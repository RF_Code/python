import re

if re.match("^[A-Z][a-z][0-9]*$", "As"):
    print("Match")
else:
    print("Not match")

if re.match("^[A-Z][a-z][0-9]+$", "As4"):
    print("Match")
else:
    print("Not match")

if re.match("^[A-Z][a-z][0-9]?[A-Z]$", "As9C"):
    print("Match")
else:
    print("Not match")

if re.match("^[A-Z][a-z][0-9]{3,4}$", "As2024"):
    print("Match")
else:
    print("Not match")