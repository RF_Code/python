import re

pattern = r"banana\nbanana\tbanana" #r - plaint text

print(pattern)

pattern = r"banana"
text = r"aplebananastawberybanana"

print(re.match(pattern, text)) #return true (default) when text starts with pattern

if re.match(r".*" + pattern + r".*", text):
    print("match")
else:
    print("Not match")

print(re.match(r".*" + pattern + r".*", text))

if re.search(pattern, text):
    print("Pattern exist in text")
else:
    print("Pattern not exist in text")

print(re.findall(pattern, text))

fit = re.search(pattern, text)

if fit:
    print(fit.group())
    print(fit.start())
    print(fit.end())
    print(fit.span())

text2 = re.sub(pattern, r"blueberry", text)
print(text2)