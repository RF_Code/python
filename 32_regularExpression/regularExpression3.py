import re

match = re.match(r"^(Hello) ((?:Wor)(?P<first>ld))+(!|\.)$", "Hello World.")

if match:
    print("Match")
    print(match.group())
    print(match.group(0))
    print(match.group(1))
    print(match.group(2))
    print(match.group(3))
    print(match.groups())
    print(match.group("first"))
else:
    print("Not match")