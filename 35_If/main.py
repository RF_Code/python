print("true") if 1 > 2 else print("not true")

a = "even" if 3 % 2 == 0 else "not even"
print(a)

for i in range(10):
    if i > 5:
        pass
else:
    print("End")

try:
    a = 5/1
except ZeroDivisionError:
    print("Error")
else:
    print("Finish")