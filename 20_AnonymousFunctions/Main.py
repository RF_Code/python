def foo(function, x):
    return function(x)

print(foo(lambda x: x * x, 3))

def square(x):
    return x * x

print(square(2))

v = lambda x: x * x
print(v(5))

z = (lambda x: x * x)(5)
print(z)

lam = lambda x, z: x * z
print(lam(3,4))