values = [-4, 6, 0, 100, 1, 1, 2, -40, 8, 99, 34, 26]
#return [-40, -4, 0, 1, 1, 2, 6, 8, 26, 34, 99, 100 ]

def sorted(values):
    index = 0
    is_sorted_continues = False
    while index <= len(values) - 2:
        current_value = values[index]
        next_value = values[index + 1]
        if current_value > next_value:
            values[index] = next_value
            values[index + 1] = current_value
            is_sorted_continues = True
        index += 1
    if is_sorted_continues:
        sorted(values)
    return values

print(sorted(values))