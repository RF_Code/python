values = list(range(10))
print(values)

newValues = [i * 2 for i in values]
newValues2 = [i + 2 for i in values if i % 2 == 0]
print("\nNew List:", newValues)
print("\nNew List2:", newValues2)