class Test:
    _values = [] #private variable
    __values2 = []  # more private variable

    def add(self,arg):
        self._values.append(arg)

    def remove(self):
        if len(self._values) > 0:
            return self._values.pop(len(self._values) - 1)
        else:
            return

test = Test()

test.add("a")
test.add("b")
test.add("c")
test._values.append("x")
#test.__values.append("x") # throw error
test._Test__values2.append("x")
print(test.remove())

print(test._values)
print(test._Test__values2)