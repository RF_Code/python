dictionary = {1: "Hello", 2: ":)", 4: ":P"}

print(dictionary[1])
print(dictionary[4])
dictionary[3] = "xd"
dictionary[5] = False
dictionary[6] = 6
print(dictionary)
dictionary["a"] = 1
print(dictionary["a"])
print(dictionary)

#print(dictionary[8])
print(dictionary.get(8, "Index not exist"))
print(dictionary.get(6, "Index not exist"))

print("\nKeys:")
for item in dictionary:
    print(item)

print("\nKeys And Values:")
for key, value in dictionary.items():
    print(key, value)

print("\nItems:")
for item in dictionary.values():
    print(item)

print("\nRemove:")
print(dictionary.pop(1))
print(dictionary)