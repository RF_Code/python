index = 0

while index < 5:
    print("Hi ;)")
    index += 1

index = 0
while True:
    print("Hi ;)" + str(index))
    index += 1
    if index == 5:
        break

index = 0
while index <= 5:
    print("*" * index)
    index += 1

index = 0
while True:
    index += 1
    if index % 2 == 1:
        continue
    print(index)
    if index > 15:
        break

print(";P")
