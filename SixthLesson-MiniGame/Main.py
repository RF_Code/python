from random import randint

value = randint(1, 10)

indexTrial = 1

while True:
    indexTrial += 1
    proposeValue = int(input("Write value between 1 - 10:"))
    if proposeValue == value:
        break
    if proposeValue > value:
        print("Try again ;) Correct value is less")
    else:
        print("Try again ;) Correct value is bigger")

print(f"You won, you tried: {indexTrial} times. Congratulation ;)")
