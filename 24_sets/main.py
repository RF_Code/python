values = {0, 1, 2, 3, 4}
valuesSet = set(["a", "b", "c"])

print(values)
print(valuesSet)

values.add(5)
print(values)

values.remove(0)
print(values)

values.add(5)
print(values)

print(1 in values)
print("a" in values)

values = {0, 1, 2, 3, 4}
v = {3, 4, 5, 6}

print(values | v)
print(values & v)
print(values - v)
print(values ^ v)