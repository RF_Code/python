values = [2, 10, 12, 15, 20, 25, 30, 35]
print(values)

#Map

a = map(lambda x: x * 2, values)
print("Map: ", list(a))

#Fiters

x = filter(lambda x: x % 2 == 0, values)
print("Filter: ", list(x))