a, b = [2, 5]
print("a: ",a, "b: ",b)

x = 10
y = 20

x, y = y, x

print("x: ",x, "y: ",y)

start, *all, end = (1, 2, 3, 4, 5, 6, 7)
print(start, all, end)