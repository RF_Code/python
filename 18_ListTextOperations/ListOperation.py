values = [10, 20, 25, 35, 40]

if all([i % 2 == 0 for i in values]):
    print("All numbers are even")
else:
    print("Some numbers aren't even")

if any([i % 2 == 0 for i in values]):
    print("At least one number is even")
else:
    print("No number is even")

for i in enumerate(values):
    print("{0}. {1}".format(i[0] + 1, i[1]))