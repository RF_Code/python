def division(x, y):
    assert y != 0, "y == 0"
    if y == 0:
        raise ZeroDivisionError("Can not divide by 0")
    print(x / y)

try:
    division(3,0)
except ZeroDivisionError:
    print("Error")
    raise
