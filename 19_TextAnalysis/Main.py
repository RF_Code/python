import string

file = open("text.txt", "r")
text = file.read()
file.close()

def countLetter(txt, letter):
    count = 0
    for z in txt:
        if z == letter:
            count += 1

    return count
print(text.count("b"))
print(countLetter(text.lower(), "b"))

for z in string.ascii_lowercase: #'abcdefghijklmnopqrstuvwxyz'
    count = countLetter(text.lower(), z)
    percent = 100 * count / len(text.replace(" ", "").lower())
    print("{0} - {1} - {2}%".format(z, count, round(percent, 2)))