class Man:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def introduction(self, greatings = "Hi"):
        return "{0}, my name is {1}. I have {2} y.o".format(greatings,self.name, self.age)

men = Man("X", 24)
print(men.introduction())
print(men.introduction("Hello"))