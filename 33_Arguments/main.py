def foo(arg, arg1=0):
    print(arg, arg1)

foo(1)

def foo2(arg, *args, **kwargs):
    print(arg, args, kwargs)
    for x in args:
        print(x)

foo2("Hi", 1, 2, 3, 4, a="XYZ", z=2020)