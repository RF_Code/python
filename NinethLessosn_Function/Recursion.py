def foo(x):
    return x * x

v = foo #v - delegete
print(v(4))

def foo2(func, x):
    return func(x) * x

print(foo2(foo, 5))

def factorial(x):
    if x <= 1:
        return 1
    else:
        return x * factorial(x - 1)

print(factorial(5))