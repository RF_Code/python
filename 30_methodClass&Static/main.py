class Man:
    def __init__(self, name):
        self.name = name

    def introduce(self):
        print("My name is {0}".format(self.name))

    @classmethod
    def new_man(cls, name):
        return cls(name)

    @staticmethod
    def greeting(arg):
        print("Hi {0}".format(arg))

man = Man.new_man("X")
Man.greeting("YXZ")
print(man.introduce())