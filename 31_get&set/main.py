class AccountBank:
    __state = 0

    @property 
    def stateAccount(self):
        return self.__state

    @stateAccount.getter
    def stateAccount(self):
        return "state: {0} PLN".format(self.__state)

    @stateAccount.setter
    def stateAccount(self, value):
        self.__state += value

acc = AccountBank()
print(acc.stateAccount)

acc.stateAccount = 50

print(acc.stateAccount)