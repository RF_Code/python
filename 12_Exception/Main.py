x = 12
y = 3

try:
    print(x / y)
    list = []
    print(list[0])
    print(x + ":)")
#except ZeroDivisionError:
#    print("You can't divide by 0")
#except TypeError:
#    print("Concatenation value type error")
except (ZeroDivisionError, TypeError):
    print("Errors 1 of 2")
except:
    print("Others errors")
finally:
    print("End")
