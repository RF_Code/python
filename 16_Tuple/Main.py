tuple = (2, 4, 8, 16, 32, 64, 128)
print(tuple[0])
print(tuple[6])

print("Items:", tuple.count(2))
print("Index", tuple.index(64))

print("\nClippings_______________")
print(tuple)
print(tuple[0:3])
print(tuple[3:len(tuple)])
print(tuple[-4:-2])

print(tuple[0:len(tuple):2])
print(tuple[0::2])
print(tuple[2:])
print(tuple[:4])
print(tuple[:4:-1])
print(tuple[::-1])