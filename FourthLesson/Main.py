age = 19
money = 10

if age >= 18 and money >= 35:
    print("Welcome ;)")
else:
    print("You don't go in ;(")

if age >= 18 or money >= 35:
    print("Welcome ;)")
else:
    print("You don't go in ;(")

if not age >= 18 or money >= 35:
    print("Welcome ;)")
else:
    print("You don't go in ;(")

if True or False and not False: #mathematical arithmetic: or as +, and as *
    print("True")
else:
    print("False")